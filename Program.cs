using System;
using System.Collections.Generic;

namespace Atividade1
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean control = true;
            List<Heroi> heroes = new List<Heroi>();
            while (control)
            {
                Console.WriteLine("1 - Cadastar novo Heroi");
                Console.WriteLine("2 - Remover Heroi");
                Console.WriteLine("3 - Listar");
                Console.WriteLine("4 - Sair");
                String selecao = Console.ReadLine();
                switch (int.Parse(selecao))
                {
                    case 1:
                        Console.WriteLine("Digite o nome:");
                        String nome = Console.ReadLine();
                        Console.WriteLine("Digite o Grupo:");
                        String grupo = Console.ReadLine();
                        heroes.Add(new Heroi(nome, grupo));
                        Console.WriteLine("Novo Heroi cadastrado -> " + "Nome: " + nome + " Grupo: " + grupo);
                        break;
                    case 2:
                        Console.WriteLine("Qual nome do Heroi deseja deletar? ");
                        String nomeBusca = Console.ReadLine();
                        foreach (Heroi iterator in heroes)
                        {
                            if(iterator.Nome == nomeBusca)
                            {
                                heroes.Remove(iterator);
                                Console.WriteLine("Heroi removido com sucesso.");
                                break;
                            }
                        }
                        break;
                    case 3:
                        foreach (Heroi iterator in heroes)
                        {
                            Console.WriteLine(iterator);
                        }
                        break;
                    default:
                        control = false;
                        break;
                }
            }
        }
    }
}
