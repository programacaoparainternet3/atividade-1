using System;
using System.Collections.Generic;
using System.Text;

namespace Atividade1
{
    class Heroi
    {
        public String Nome { get; set; }
        public String Grupo { get; set; }

        public Heroi(string nome, string grupo)
        {
            this.Nome = nome;
            this.Grupo = grupo;
        }


        public override string ToString()
        {
            return base.ToString() +": " + "Nome: " + this.Nome + " Grupo: " + this.Grupo;
        }

        public static implicit operator Heroi(List<Heroi> v)
        {
            throw new NotImplementedException();
        }
    }
}
